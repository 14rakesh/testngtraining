package testNGTests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Test1 {
	
	 WebDriver driver;
	 	
	    @BeforeMethod
	    public void beforeMethod() {		
	        //Create a new instance of the Firefox driver		
	        driver = new FirefoxDriver(); 
	       
	       /* @FindBy(xpath="//a[@class='ld-login ld-login ld-login-text ld-login-button ld-button']")
			WebElement loginButton;
		 	@FindBy(id="user_login")
			WebElement userName;*/
	        //Open browser
	        driver.get("https://www.training-support.net");
	      /*  driver.findElement(By.xpath("//a[@class='ld-login ld-login ld-login-text ld-login-button ld-button']")).click();
	        driver.findElement(By.id("user_login")).sendKeys("root");
	        driver.findElement(By.id("user_login")).sendKeys("user_pass");
	        driver.findElement(By.id("wp-submit")).click();*/
	    }
	    
	    @Test
	    public void exampleTestCase() {
	        // Check the title of the page
	        String title = driver.getTitle();
		    //Print the title of the page
	        System.out.println("Page title is: " + title);
	        //Assertion for page title
	        Assert.assertEquals(title, "Training Support");
	    }
	    
	    @AfterMethod
	    public void afterMethod() {
	       //Close the browser
	        driver.quit();
	    }
}
