package testNGTests;

import java.sql.Driver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity5 {
	WebDriver driver;
	@BeforeClass
	public void initiateDriver() {
		
		driver= new FirefoxDriver();
		driver.get("https://www.training-support.net/selenium/target-practice");
	}
	@AfterClass
	public void closeDriver() {
		driver.close();
	}
	@Test
	public void pageTitle() {
		String title = driver.getTitle();
        System.out.println("Title is: " + title);    	
        Assert.assertEquals(title, "Target Practice");
	}
	@Test (groups ={"HeaderTests"})
	public void headerTest() {
        WebElement headerText3 = driver.findElement(By.cssSelector("h3#third-header"));    	
        Assert.assertEquals(headerText3.getText(), "Third header");
	}
	@Test (groups ={"HeaderTests"})
    public void HeaderTest5() {    	
        WebElement header5 = driver.findElement(By.cssSelector("h3#ui green header"));
        String headercolor = header5.getCssValue("color");
        Assert.assertEquals(header5.getCssValue("color"), "rgb(251, 189, 8)");	
    }
	@Test (groups ={"ButtonTests"})
    public void ButtonTest1() {    	
        WebElement button1 = driver.findElement(By.cssSelector("button.olive"));	
        Assert.assertEquals(button1.getText(), "Olive");	
    }
	@Test (groups ={"ButtonTests"})
    public void ButtonTest2() {
        WebElement button2 = driver.findElement(By.cssSelector("button.brown"));	
        Assert.assertEquals(button2.getCssValue("color"), "rgb(255, 255, 255)");
	
    }
}
