package testNGTests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.SkipException;
import org.testng.TestException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestngActivity2 {
	
	 WebDriver driver;
	 	
	    @BeforeClass
	    public void beforeMethod() {		
	        //Create a new instance of the Firefox driver		
	        driver = new FirefoxDriver(); 
	        //Open browser
	        driver.get("https://www.training-support.net/selenium/target-practice");
	      /*  driver.findElement(By.xpath("//a[@class='ld-login ld-login ld-login-text ld-login-button ld-button']")).click();
	        driver.findElement(By.id("user_login")).sendKeys("root");
	        driver.findElement(By.id("user_login")).sendKeys("user_pass");
	        driver.findElement(By.id("wp-submit")).click();*/
	    }
	    
	    @Test(priority =1)
	    public void checkTitle() {
	        // Check the title of the page
	        String title = driver.getTitle();
		    //Print the title of the page
	        System.out.println("Page title is: " + title);
	        //Assertion for page title
	        Assert.assertEquals(title, "Target Practice");
	    }
	    @Test (priority =2)
	    public void objectisdisplayed() {
	    	try {
	    		if (driver.findElement(By.xpath("//i[@class='bullseye icon']")).isDisplayed()) {
	    			WebElement black_button = driver.findElement(By.xpath("//i[@class='bullseye icon']"));
	    			Assert.assertTrue(black_button.isDisplayed());
			    	Assert.assertEquals(black_button.getTagName(), "black");
	    		}
	    	}catch (Exception e){
	    		System.out.println("======================");  
	    		System.out.println(e);
	    		Reporter.log("Webelement is not avaialble");
	    		System.out.println("======================");
	    		//throw new TestException("Element is not available");
	    	}
	    	System.out.println("Execute next step");     
	    }
	    @Test(priority =3, enabled=false)
	    public void SkipwithoutReporting() {
	    	  System.out.println("Skip the test without reproing in Result reprot ");
	        
	    }
	    
	    @Test(priority =4)
	    public void SkipTest() throws SkipException {
	    	String x="Test";
	    	if (x.equals("Test")) {
	    		throw new SkipException("Skip - This is not ready for testing");
	    	}else {
	    		System.out.println("Execute the statement");
	    	}
	    }
	    @AfterClass
	    public void afterMethod() {
	       //Close the browser
	        driver.quit();
	    }
}
