package testNGTests;

import java.sql.Driver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class Activity6 {
	WebDriver driver;
	@BeforeClass
	public void initiateDriver() {
		
		driver= new FirefoxDriver();
		driver.get("https://www.training-support.net/selenium/login-form");
	}
	@AfterClass
	public void closeDriver() {
		driver.close();
	}
	@Test
	@Parameters({"sUserName","sPassword"})
	public void applicationLogin(String sUserName,String sPassword) {
		WebElement objuserName = driver.findElement(By.id("username"));
		WebElement objPassword = driver.findElement(By.id("password"));
		WebElement LoginButton = driver.findElement(By.xpath("//button[@class='ui button']"));
		WebElement message = driver.findElement(By.id("action-confirmation"));
		objuserName.sendKeys(sUserName);
		objPassword.sendKeys(sPassword);
		LoginButton.click();
		String msgActual = message.getText();
		Assert.assertEquals(msgActual, "Welcome Back, admin");
	}
}
